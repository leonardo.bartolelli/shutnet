# ShutNet

---

# What's this?

ShutNet, is a virus that will automaticly delete every system event log and network log from every ip connected to your wifi.

# How does it work? 

You just need to download the file ShutNet.bat and run it ON WINDOWS, after that the code will run.
However, to run the script you must need the administrative privileges.

# Will it cause any harm to any device?

No, it will not, however, it will delete the system log, which can be useful, but not essential.

---

# Instructions

- Download and extract the zip / tar.gz file
- Make sure you are connected to a wifi connection
- Right click on the ShutNet.bat file
- - Click on "Open as administator"
- Enjoy!



If you have any issue please create an issue in the 'Issues' section.
